<?php

use App\Http\Controllers\CommonController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ItemController::class, 'index'])->name('main');
Route::post('/create', [ItemController::class, 'addItem'])->name('item.add');
Route::delete('/delete', [ItemController::class, 'destroyItem'])->name('item.delete');
