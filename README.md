## Install

composer install

cp .env.example .env

alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'

sail up

sail npm install

sail npm run dev

sail composer install

sail artisan key:generate

sail artisan migrate

sail artisan storage:link

sail artisan ide-helper:generate

sail artisan ide-helper:models