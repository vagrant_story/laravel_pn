<?php

namespace App\Http\Controllers;

use App\ApiTools\ApiToolsInterface;
use App\Services\ItemService;
use App\Services\ItemServiceInterface;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ItemController extends Controller
{
    /**
     * @var ApiToolsInterface
     */
    private $apiTools;

    /**
     * @var ItemServiceInterface
     */
    private $itemService;

    /**
     * ItemController constructor
     *
     * @param ApiToolsInterface $apiTools
     * @param ItemServiceInterface $itemService
     */
    public function __construct(
            ApiToolsInterface $apiTools,
            ItemServiceInterface $itemService
    ) {
        $this->apiTools = $apiTools;
        $this->itemService = $itemService;
    }

    /**
     * @return View
     */
    public function index()
    {
        $orders = $this->itemService->getAll(ItemService::ORDER);
        $products = $this->itemService->getAll(ItemService::PRODUCT);

        return view('home', compact('orders', 'products'));
    }

    /**
     * Add item from API
     *
     * @return RedirectResponse
     * @throws Exception
     */
    public function addItem(): RedirectResponse
    {
        $login = $this->apiTools->loginToApi();
        if ($login['status'] == 200) {
            $parse = $this->apiTools->getString($login['token']);
            if ($parse['status'] == 200) {
                $response = $this->itemService->saveItem($parse['content']);
            } else {
                $response = $parse['error'];
            }
        } else {
            $response = $login['error'];
        }

        return redirect()->route('main')->with('message', $response->content());
    }

    /**
     * Delete item
     *
     * @param Request $item
     *
     * @return RedirectResponse
     */
    public function destroyItem(Request $item)
    {
        $response = $this->itemService->deleteItem($item->all());

        return redirect()->route('main')->with('message', $response->content());
    }
}
