<?php

declare(strict_types=1);

namespace App\Services;

use App\Repositories\BaseRepositoryInterface;
use Illuminate\Support\Collection;

interface ItemServiceInterface
{
    /**
     * Save item to database
     *
     * @param array $content
     * @return mixed
     */
    public function saveItem(array $content);

    /**
     * Delete item from database
     *
     * @param array $data
     * @return mixed
     */
    public function deleteItem(array $data);

    /**
     * @param string $type
     * @return BaseRepositoryInterface
     */
    public function getItemRepository(string $type): BaseRepositoryInterface;

    /**
     * @param string $type
     * @return mixed
     */
    public function getAll(string $type);

}
