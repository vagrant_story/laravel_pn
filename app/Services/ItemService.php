<?php

declare(strict_types=1);

namespace App\Services;

use App\Repositories\BaseRepositoryInterface;
use App\Repositories\OrderRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;

class ItemService implements ItemServiceInterface
{
    const ORDER = 'order';
    const PRODUCT = 'product';

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * ItemRepository Constructor
     *
     * @param  OrderRepositoryInterface  $orderRepository
     * @param  ProductRepositoryInterface  $productRepository
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        ProductRepositoryInterface $productRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
    }

    public function getAll(string $type)
    {
        $repositoryItem = $this->getItemRepository($type);

        return $repositoryItem->all();
    }


    /**
     * @inheritDoc
     *
     * @throws \Exception
     */
    public function saveItem(array $content)
    {
        $repositoryItem = $this->getItemRepository($content['type']);

        return $repositoryItem->save($content['item']);
    }

    /**
     * @inheritDoc
     *
     * @throws \Exception
     */
    public function deleteItem(array $data)
    {
        $repositoryItem = $this->getItemRepository($data['type']);

        return $repositoryItem->delete($data['id']);
    }

    /**
     * @inheritDoc
     *
     * @throws \Exception
     */
    public function getItemRepository(string $type): BaseRepositoryInterface
    {
        switch ($type) {
            case self::ORDER:
                return $this->orderRepository;
                break;
            case self::PRODUCT:
                return $this->productRepository;
                break;
            default:
                throw new \Exception('Can\'t recognize type of item');
        }
    }
}
