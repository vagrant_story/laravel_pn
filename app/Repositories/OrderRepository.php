<?php

namespace App\Repositories;

use App\Models\Order;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;


class OrderRepository implements BaseRepositoryInterface, OrderRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function all()
    {
       return Order::all();
    }

    /**
     * @inheritDoc
     *
     * @throws ValidationException
     */
    public function save(array $item): JsonResponse
    {
        $rules = [
            'id'             => 'required',
            'total'          => 'required',
            'shipping_total' => 'required',
            'create_time'    => 'required',
            'timezone'       => 'required',
        ];

        $validator = Validator::make($item, $rules);
        $itemValidated = $validator->validate();

        $order = $this->getByOrderIdWithTrashed($itemValidated['id']);

        if ($order) {
            if ($order->trashed()) {
                $order->restore();
            } else {
                return new JsonResponse([
                    'error' => 'Order already in system.'
                ]);
            }
        } else {
            Order::create(
                [
                    'order_id'       => $itemValidated['id'],
                    'total'          => $itemValidated['total'],
                    'shipping_total' => $itemValidated['shipping_total'],
                    'create_time'    => $itemValidated['create_time'],
                    'timezone'       => $itemValidated['timezone'],
                ]
            );
        }

        return new JsonResponse([
            'success' => 'Order successfully added.',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function delete($id): JsonResponse
    {
        $response = new JsonResponse();
        $delete = Order::findOrFail($id)->delete();
        $delete
            ? $response->setData(['success' => 'Order successfully deleted'])
            : $response->setData(['error' => 'Order has not been deleted']);

        return $response;
    }

    /**
     * @param $id
     *
     * @return Order
     */
    public function getByOrderIdWithTrashed($id): Order
    {
        return Order::withTrashed()
            ->where('order_id', $id)
            ->first();
    }
}
