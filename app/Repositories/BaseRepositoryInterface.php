<?php

declare(strict_types=1);

namespace App\Repositories;

use Illuminate\Http\JsonResponse;

interface BaseRepositoryInterface
{
    /**
     * Save item to database
     *
     * @param array $item
     *
     * @return JsonResponse
     */
    public function save(array $item);

    /**
     * Delete item by id
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id);

    /**
     * Get all items
     *
     * @return mixed
     */
    public function all();
}
