<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ProductRepository implements BaseRepositoryInterface, ProductRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function all()
    {
        return Product::all();
    }

    /**
     * @inheritDoc
     *
     * @throws ValidationException
     */
    public function save(array $item): JsonResponse
    {
        $rules = [
                'SKU' => 'required',
                'title'   => 'required',
                'image' => 'required',
                'extension' => 'required',
        ];

        $validator = Validator::make($item, $rules);
        $itemValidated = $validator->validate();

        $product = $this->getBySkuWithTrashed($itemValidated['SKU']);

        if ($product) {
            if ($product->trashed()) {
                $product->restore();
            } else {
                return new JsonResponse([
                    'error' => 'Product already in system.'
                ]);
            }

        } else {
            Product::create($itemValidated);
        }

        return new JsonResponse([
            'success' => 'Product successfully added.',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function delete($id): JsonResponse
    {
        $response = new JsonResponse();
        $delete = Product::findOrFail($id)->delete();
        $delete
            ? $response->setData(['success' => 'Product successfully deleted'])
            : $response->setData(['error' => 'Product has not been deleted']);

        return $response;
    }

    /**
     * @param $id
     *
     * @return Product
     */
    public function getBySkuWithTrashed($id): Product
    {
        return Product::withTrashed()
            ->where('SKU', $id)
            ->first();
    }
}
