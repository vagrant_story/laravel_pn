<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'order_id',
        'total',
        'shipping_total',
        'create_time',
        'timezone',
    ];

    /**
     * Return Carbon TimeString from Timestamp
     *
     * @param $value
     * @return string
     */
    public function getCreateTimeAttribute($value)
    {
        return Carbon::createFromTimestamp($value)->toDateTimeString();
    }
}
