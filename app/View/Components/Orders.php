<?php

namespace App\View\Components;

use Illuminate\Support\Collection;
use Illuminate\View\Component;

class Orders extends Component
{
    /**
     * Title
     *
     * @var string
     */
    public $title;

    /**
     * Array of orders
     *
     * @var Collection
     */
    public $orders;

    /**
     * Create a new component instance.
     *
     * @param string $title
     * @param Collection $orders
     */
    public function __construct(string $title, Collection $orders)
    {
        $this->title = $title;
        $this->orders = $orders;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.orders');
    }
}
