<?php

namespace App\View\Components;

use Illuminate\Support\Collection;
use Illuminate\View\Component;

class Products extends Component
{
    /**
     * Title
     *
     * @var string
     */
    public $title;

    /**
     * Array of products
     *
     * @var Collection
     */
    public $products = [];

    /**
     * Create a new component instance.
     *
     * @param string $title
     * @param Collection $products
     */
    public function __construct(string $title, Collection $products)
    {
        $this->title = $title;
        $this->products = $products;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.products');
    }
}
