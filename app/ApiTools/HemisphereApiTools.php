<?php

namespace App\ApiTools;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;

class HemisphereApiTools implements ApiToolsInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $token = '';

    /**
     * @var string
     */
    private $error = 'Can\'t connect to the APi';

    /**
     * @param  Client  $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     *  Login to API and get token
     *
     * @return string[]
     * @throws GuzzleException
     */
    public function loginToApi(): array
    {
        $result = [
            'status' => '',
            'token' => '',
            'error' => '',
        ];

        $response = $this->client->post(env('APP_API_URL_SIGN'), [
            'form_params' => [
                'client_id' => env('APP_API_LOGIN'),
                'client_secret' => env('APP_API_PASSWORD'),
            ],
        ]);

        $result['status'] = $response->getStatusCode();

        if ($result['status'] == 200) {
            $this->token = $result['token'] = json_decode((string)$response->getBody(), true)['access_token'];
        } else {
            $result['error'] = new JsonResponse([
                'error' => $this->error,
            ]);
        }

        return $result;
    }

    /**
     * Get decoded item
     *
     * @return string[]
     * @throws GuzzleException
     */
    public function getString(): array
    {
        $result = [
            'status' => '',
            'content' => '',
            'type' => '',
            'error' => '',
        ];

        $response = $this->client->post(env('APP_API_URL_CONTENT'), [
            'headers' => [
                'Authorization' => $this->token,
            ],
        ]);

        $result['status'] = $response->getStatusCode();

        if ($result['status'] == 200) {
            $content = json_decode((string)$response->getBody(), true);
            $result['content'] = $this->parseString($content);
        } else {
            $result['error'] = new JsonResponse([
                'error' => $this->error,
            ]);
        }

        return $result;
    }

    /**
     * Extract fields from the string
     *
     * @param  string  $content
     * @return string[]
     */
    public function parseString(string $content): array
    {
        $result = [
            'type' => '',
            'item' => '',
        ];
        $fields = [];

        $itemType = explode(':', $content);
        $itemContentArray = explode('||', $itemType[1]);

        foreach ($itemContentArray as $item) {
            preg_match('/(.*?)\{(.*)\}.*/i', $item, $match);

            if (strripos($match[1], 'image') !== false) {
                $columnName = 'extension';
                $columnValue = substr($match[1], strpos($match[1], ';') + 1);
                $fields[$columnName] = $columnValue;
                $fields['image'] = $match[2];
                continue;
            }

            $fields[$match[1]] = $match[2];
        }

        $result['type'] = $itemType[0];
        $result['item'] = $fields;

        return $result;
    }
}
