<?php

namespace App\ApiTools;

interface ApiToolsInterface
{
    /**
     * login to Api
     *
     * @return mixed
     */
    public function loginToApi();

    /**
     * Get the new item(s) from Api
     *
     * @return mixed
     */
    public function getString();

    /**
     * parse the string
     *
     * @return mixed
     */
    public function parseString(string $content);
}