@extends('app')

@section('content')
    <div class="messages__section"
    <x-messages/>
    </div>
    <div class="control__section">
        <x-create-button/>
    </div>
    <div class="block__section">
        <div class="container">
            <div class="row">
                <x-orders title="Orders" :orders="$orders"/>
                <x-products title="Products" :products="$products"/>
            </div>
        </div>
    </div>
@endsection
