<div class="block">
    <h3>{{ $title }}</h3>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">order_id</th>
            <th scope="col">total</th>
            <th scope="col">shipping_total</th>
            <th scope="col">create_time</th>
            <th scope="col">timezone</th>
            <th scope="col">delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach($orders as $order)
            @php
                $item = ['type' => 'order', 'id' => $order->id];
            @endphp
            <tr>
                <td>{{ $order->order_id }}</td>
                <td>{{ $order->total }}</td>
                <td>{{ $order->shipping_total }}</td>
                <td>{{ $order->create_time }}</td>
                <td>{{ $order->timezone }}</td>
                <td><x-delete-button type="order" :item="$item"/></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
