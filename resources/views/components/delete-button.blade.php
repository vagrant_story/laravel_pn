<div class="delete">
    <form action="{{ route('item.delete', $item) }}" method="post">
        @csrf
        @method('DELETE')
        <input type="submit" value="Del">
    </form>
</div>
