<div class="row message">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if($message = Session::get('message'))
        @foreach(json_decode(session()->get('message'), true) as $key => $value)
            @if ($key === 'error')
                <div class='alert alert-danger'>
            @elseif ($key === 'success')
                <div class='alert alert-success'>
            @else
                <div class='alert alert-info'>
            @endif
            {{ strtoupper($key) . ': ' . $value }}
        @endforeach
    @endif
</div>
