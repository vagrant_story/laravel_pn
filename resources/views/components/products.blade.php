<div class="block">
    <h3>{{ $title }}</h3>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">title</th>
            <th scope="col">SKU</th>
            <th scope="col">image</th>
            <th scope="col">delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            @php
                $item = ['type' => 'product', 'id' => $product->id];
            @endphp
            <tr>
                <td>{{ $product->title }}</td>
                <td>{{ $product->SKU }}</td>
                <td><img src="data:image/png;base64, {{ $product->image }}" alt="Image Preview"/></td>
                <td><x-delete-button :item="$item"/></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
